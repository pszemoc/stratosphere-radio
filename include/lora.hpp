#pragma once
#include <exception>
#include <stdexcept>

namespace lora {
    namespace cmd {
        namespace sys {
            constexpr char get_ver[]    = "sys get ver\r\n";
            constexpr char sleep[]      = "sys sleep ";
        }
        namespace radio {
            constexpr char rx[]             = "radio rx ";
            constexpr char tx[]             = "radio tx ";
            constexpr char set_mod[]        = "radio set mod ";
            constexpr char set_freq[]       = "radio set freq ";
            constexpr char set_pwr[]        = "radio set pwr ";
            constexpr char set_sf[]         = "radio set sf ";
            constexpr char set_bw[]         = "radio set bw ";
            constexpr char set_cr[]         = "radio set vr ";
            constexpr char get_snr[]        = "radio get snr\r\n";
        }
    }

    class config_error : public std::runtime_error {
    public:
        config_error(const char* what_arg) : std::runtime_error(what_arg) {}
    };

    class network_error : public std::runtime_error {
    public:
        network_error(const char* what_arg) : std::runtime_error(what_arg) {}
    };

    class device_error : public std::runtime_error {
    public:
        device_error(const char* what_arg) : std::runtime_error(what_arg) {}
    };

    class Radio {
    private:
        int serial;
    public:
        Radio();
        ~Radio();
        
        void send_cmd(const char* cmd);
        void send_cmd(const char* cmd, const char* arg);
    };
}