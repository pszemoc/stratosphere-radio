#include "lora.hpp"
#include <unistd.h>
#include <string.h>

lora::Radio::Radio() {

}

lora::Radio::~Radio() {

}

void lora::Radio::send_cmd(const char* cmd) {
    write(serial, cmd, strlen(cmd));
}

void lora::Radio::send_cmd(const char* cmd, const char* arg) {
   int buffer_length = strlen(cmd) + strlen(arg) + 3; // \r\n\0
   char* buffer = new char[buffer_length];
   memset(buffer, 0, buffer_length);
   strcpy(buffer, cmd);
   strcat(buffer, arg);
   strcat(buffer, "\r\n");
   write(serial, buffer, buffer_length);
}

